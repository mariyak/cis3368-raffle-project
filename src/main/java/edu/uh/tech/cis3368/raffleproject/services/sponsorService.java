package edu.uh.tech.cis3368.raffleproject.services;

import edu.uh.tech.cis3368.raffleproject.entities.Sponsors;
import edu.uh.tech.cis3368.raffleproject.repositories.SponsorsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class sponsorService {

    SponsorsRepository repository;


    @Autowired
    public sponsorService(SponsorsRepository repository) {
        System.out.println("in the Selector service ctor");
        this.repository = repository;
    }

    public Iterable<Sponsors> findAll() {
        return repository.findAll();
    }


}





