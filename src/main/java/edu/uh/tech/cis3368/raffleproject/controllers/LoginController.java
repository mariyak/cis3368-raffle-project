package edu.uh.tech.cis3368.raffleproject.controllers;

import edu.uh.tech.cis3368.raffleproject.repositories.ControlledScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Component
public class LoginController implements Initializable, ControlledScreen {


    ScreensController myController;



    @FXML
    Label message;

    @FXML
    Button loginButton;
    @FXML
    PasswordField passwordField;
    @FXML
    TextField usernameField;

    boolean correctInfo = false;




    //This method is used to switch back to the Student Page
    @FXML
    public void handleSubmit(ActionEvent event) throws IOException {


        String p = passwordField.getText();
        String n = usernameField.getText();

        if (n.equals("admin") && p.equals("root")) {
            correctInfo = true;
        }

        if (correctInfo)
        {

            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/sponsorSelection.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }

        else {
            message.setText("Your username or password information is incorrect!");
        }

    }








    @FXML
    public void initialize(URL url, ResourceBundle rb) {

//end of  void initialize*/
    }








    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }



}

