package edu.uh.tech.cis3368.raffleproject.repositories;


import edu.uh.tech.cis3368.raffleproject.controllers.ScreensController;

public interface ControlledScreen {

    //injects parent screen ?
    public void setScreenParent(ScreensController screenPage);
}
