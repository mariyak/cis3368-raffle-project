package edu.uh.tech.cis3368.raffleproject;

import edu.uh.tech.cis3368.raffleproject.entities.Participants;
import edu.uh.tech.cis3368.raffleproject.entities.Sponsors;
import edu.uh.tech.cis3368.raffleproject.repositories.ParticipantsRepository;
import edu.uh.tech.cis3368.raffleproject.repositories.SponsorsRepository;
import edu.uh.tech.cis3368.raffleproject.services.RandomSelectorService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class RaffleProjectApplication extends Application {

    private ConfigurableApplicationContext springContext;
    private Parent rootNode;
    @Autowired
    private ParticipantsRepository participantsRepository;
    @Autowired
    private SponsorsRepository sponRepo;
    @Autowired
    private RandomSelectorService randomSelectorService;





    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        springContext = SpringApplication.run(RaffleProjectApplication.class);
        springContext.getAutowireCapableBeanFactory().autowireBean(this);
        FXMLLoader fxmlloader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/login.fxml"));
        fxmlloader.setControllerFactory(springContext::getBean);
        rootNode = fxmlloader.load();
    }



    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(rootNode));
        stage.show();

        //using participant repo
        Iterable<Participants> participantsIterable = participantsRepository.findAll();
        for (Participants participant : participantsIterable) {
            System.out.println(participant);
        }


        //using spon repo
        Iterable<Sponsors> sponIt = sponRepo.findAll();
        for (Sponsors spon : sponIt) {
            System.out.println(spon);
        }



    }

    @Override
    public void stop() throws Exception {
        springContext.close();
    }



}
