package edu.uh.tech.cis3368.raffleproject.repositories;

import edu.uh.tech.cis3368.raffleproject.entities.Sponsors;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SponsorsRepository extends CrudRepository<Sponsors, Integer>{

}


