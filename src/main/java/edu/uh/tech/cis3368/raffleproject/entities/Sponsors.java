package edu.uh.tech.cis3368.raffleproject.entities;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity
public class Sponsors {
    private int id;
    private String sponsorName;
    private byte[] sponsorLogo;
    private String sponsorType;
    private String sponsorBio;
    private Integer raffleItems;

    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "SPONSOR_NAME", nullable = true, length = 24)
    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    @Basic
    @Column(name = "SPONSOR_TYPE", nullable = true, length = 100)
    public String getSponsorType() {
        return sponsorType;
    }

    public void setSponsorType(String sponsorType) {
        this.sponsorType = sponsorType;
    }

    @Basic
    @Column(name = "SPONSOR_BIO", nullable = true, length = 2000)
    public String getSponsorBio() {
        return sponsorBio;
    }

    public void setSponsorBio(String sponsorBio) {
        this.sponsorBio = sponsorBio;
    }

    @Basic
    @Column(name = "RAFFLE_ITEMS", nullable = true)
    public Integer getRaffleItems() {
        return raffleItems;
    }

    public void setRaffleItems(Integer raffleItems) {
        this.raffleItems = raffleItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sponsors sponsors = (Sponsors) o;
        return id == sponsors.id &&
                Objects.equals(sponsorName, sponsors.sponsorName) &&
                Arrays.equals(sponsorLogo, sponsors.sponsorLogo) &&
                Objects.equals(sponsorType, sponsors.sponsorType) &&
                Objects.equals(sponsorBio, sponsors.sponsorBio) &&
                Objects.equals(raffleItems, sponsors.raffleItems);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(id, sponsorName, sponsorType, sponsorBio, raffleItems);
        result = 31 * result + Arrays.hashCode(sponsorLogo);
        return result;
    }
}
