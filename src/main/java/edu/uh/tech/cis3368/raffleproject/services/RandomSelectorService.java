package edu.uh.tech.cis3368.raffleproject.services;

import edu.uh.tech.cis3368.raffleproject.entities.Participants;
import edu.uh.tech.cis3368.raffleproject.repositories.ParticipantsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class RandomSelectorService {

    ParticipantsRepository repository;

    @Autowired
    public RandomSelectorService(ParticipantsRepository repository) {
        System.out.println("in the Selector service ctor");
        this.repository = repository;
    }

    public List<Participants> findLosers(){
        return repository.findByWinnerFalse();
    }

    public Participants nextWinner(){
        Participants winner;

        List<Participants> potentialWinners = repository.findByWinnerFalse();
        // pick one at random...



        Random rnd = new Random();
        int i = rnd.nextInt(potentialWinners.size());
        return potentialWinners.get(i);
    }
}
