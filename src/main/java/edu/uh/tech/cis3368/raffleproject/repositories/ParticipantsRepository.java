package edu.uh.tech.cis3368.raffleproject.repositories;

import edu.uh.tech.cis3368.raffleproject.entities.Participants;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParticipantsRepository extends CrudRepository<Participants,Integer> {

    List<Participants> findByWinnerFalse();
    Participants findByLastNameIs(String lastname);
}
