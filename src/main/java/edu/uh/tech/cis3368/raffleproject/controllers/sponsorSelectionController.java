package edu.uh.tech.cis3368.raffleproject.controllers;

import edu.uh.tech.cis3368.raffleproject.repositories.ControlledScreen;
import edu.uh.tech.cis3368.raffleproject.entities.Sponsors;
import edu.uh.tech.cis3368.raffleproject.repositories.SponsorsRepository;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.control.Button;


@Component
public class sponsorSelectionController implements Initializable, ControlledScreen {




    private ScreensController myController;


    @Autowired
    SponsorsRepository repository;


    @FXML
    Button selectButton;
    @FXML
    Button backButton;
    @FXML
    TableView sponsorTable;




    public void handleSelect(ActionEvent event) throws Exception {


        Iterable<Sponsors> sponIt = repository.findAll();
        for (Sponsors spon : sponIt) {
            System.out.println(spon);
        }



    }



    @FXML
    public void initialize(URL url, ResourceBundle rb) {



//end void initialize*/
    }





////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////This method is used to switch back to the login page//////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    @FXML
    public void handleBackButton(ActionEvent event) throws IOException {
        Node node=(Node) event.getSource();
        Stage stage=(Stage) node.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/login.fxml"));/* Exception */
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }






    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }

}