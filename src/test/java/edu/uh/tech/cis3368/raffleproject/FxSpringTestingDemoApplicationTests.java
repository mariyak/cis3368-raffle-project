package edu.uh.tech.cis3368.raffleproject;

import edu.uh.tech.cis3368.raffleproject.entities.Participants;
import edu.uh.tech.cis3368.raffleproject.repositories.ParticipantsRepository;
import edu.uh.tech.cis3368.raffleproject.services.RandomSelectorService;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class FxSpringTestingDemoApplicationTests {

    @Resource
    private ParticipantsRepository participantsRepository;

    @Autowired
    RandomSelectorService randomSelectorService;

    @Before
    public void createData(){
        Participants participants = new Participants();
        participants.setLastName("barry");
        participants.setWinner(false);
        participantsRepository.save(participants);
    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void findLosers(){
        List<Participants> participants = randomSelectorService.findLosers();
        System.out.println(participants);
    }


    @Test
    public void isBarryThere(){
        Participants barry = participantsRepository.findByLastNameIs("barry");
        assertEquals(barry.getLastName(),"john");

    }

}
